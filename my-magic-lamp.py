#!/usr/bin/env python3

#MagicLamp 1.0 STABLE
#questo programma e' usato per controllare un relay/lamp/lamp. Esegue un controllo di esistenza su due file shut_path e interr_path. Se shut_path e'
#presente termina il programma; se interr_path è presente accende il relay/lamp/lamp.

import RPi.GPIO as GPIO
from time import sleep
import os
import sys
import signal


#numero del pin GPIO
pin1 = 15

on = False  

GPIO.setmode(GPIO.BOARD)
GPIO.setup(pin1,GPIO.OUT, initial=GPIO.HIGH)   #utilizza il relay/lamp/lamp numero 1

global rp

def terminate(x,y):
    GPIO.output(pin1, GPIO.HIGH)
    GPIO.cleanup()
    print('Terminating\n')
    sys.exit(0)

signal.signal(signal.SIGTERM, terminate)
signal.signal(signal.SIGINT, terminate)

rfPath = "/home/fox/test-python/relay/lamp/p1"

try:

    os.mkfifo(rfPath)

except OSError:
    pass


print ("MagicLamp 0.1..... Ready!")

while (1):

    rp = open(rfPath, 'r')
    response = rp.read()
    if (response == "1"):
        if (on == False):   #se non e gia accesa
            GPIO.output(pin1, GPIO.LOW) #accendi la lampada
            print ("Accendo la lampada...\n")
            on = True
    elif (response == "2"):
        if (on == True):
            print ("Spengo la lampada e termino il programma...\n")
        else:
            print ("Termino il programma")
        break

    else:
        if (on == True):
            GPIO.output(pin1, GPIO.HIGH)    #spegni la lampada
            print ("Spengo la lampada\n")
        on = False
    rp.close()
terminate(1,2) #1,2 sono valori a caso tanto per passare dei valori a terminate
