#!/usr/bin/python

# import cPickle
import os

#communicate with another process through named pipe

#one for receive command, the other for send command

rfPath = "./p1"

try:

    os.mkfifo(rfPath)

except OSError:
    pass

rp = open(rfPath, 'r')
response = rp.read()
print "P2 hear %s" % response
rp.close()
